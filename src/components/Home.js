import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import {homeDuck} from "state/homeDuck";

export const Home = () => {
    const dispatch = useDispatch();
    const greetings = useSelector(state => homeDuck.selectors.greetings(state));

    useEffect(() => {
        setTimeout(() => {
            dispatch(homeDuck.creators.loadApp());
        }, 3000)
    }, [dispatch]);

    return <Box display={'flex'} flexGrow={1} justifyContent={'center'} alignItems={'center'} >
        <Typography variant={'h1'}>
            {greetings}
        </Typography>
    </Box>
};