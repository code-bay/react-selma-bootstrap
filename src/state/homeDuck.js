import Duck from "extensible-duck";

export const homeInitialState = {
    greetings: 'Loading...'
};

const duckParams = {
    namespace: 'app',
    store: 'home',
    initialState: homeInitialState,
    selectors: {
        greetings: state => state.home.greetings
    },
    types: ['LOAD_ADD'],
    reducer: (state, action, { types }) => {
        switch (action.type) {
            case types.LOAD_ADD: return loadApp(state, action);
            default: return state
        }
    },
    creators: ({ types }) => ({
        loadApp: (payload) => ({ type: types.LOAD_ADD, payload }),
    }),
};

const loadApp = (state, ) => {
    return Object.assign({}, state, {
        greetings: 'Hi !'
    })
};

export const homeDuck = new Duck(duckParams);
