import React from 'react';
import { Provider } from 'react-redux';
import { Routes, Route } from 'react-router-dom';
import { ReduxRouter } from '@lagunovsky/redux-react-router'
import { createTheme, ThemeProvider } from '@mui/material/styles';

import {createStore, history} from 'reducers';
import {Home} from "components/Home";

const defaultTheme = createTheme();

export const App = () => {
    const store = createStore({});
    return <Provider store={store}>
        <ThemeProvider theme={defaultTheme}>
            <ReduxRouter history={history}> { /* place ConnectedRouter under Provider */ }
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route render={() => (<div>Miss</div>)} />
                </Routes>
            </ReduxRouter>
        </ThemeProvider>
    </Provider>
};