import { combineReducers } from 'redux';
import { createRouterReducer, createRouterMiddleware } from '@lagunovsky/redux-react-router'
import {configureStore} from "@reduxjs/toolkit";
import {createBrowserHistory} from "history";
import {homeDuck} from "state/homeDuck";

export const history = createBrowserHistory();

const createRootReducer = (history) => combineReducers({
    router: createRouterReducer(history),
    [homeDuck.store]: homeDuck.reducer
});

export const createStore = (preloadedState) => {
    return configureStore({
        preloadedState,
        reducer: createRootReducer(history),
        middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(
            createRouterMiddleware(history)
        )
    });
};



